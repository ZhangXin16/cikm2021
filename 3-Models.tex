%!TEX root = ./HHGN-chenchonghao.tex
\section{APPROACH}
\label{approach}

\begin{figure*}[t]
	\centering
	\includegraphics[width=\textwidth]{FEAT_figures/Framework.eps}
	\caption{The contrastive task adaptation framework.}
	\label{framework}
    \vspace*{-3mm}
	%	\vspace*{-1\baselineskip}
\end{figure*}

In this section, we introduce the problem definition in \S\ref{Problem definition}. 
Then, the overall framework of task adaptive few-shot intent recognition is described in \S\ref{Task adaptive few-shot intent recognition framework}. 
%
In particular, the contrastive learning based feature extractor and the task adaptation strategy are presented in \S\ref{Contrastive learning based feature extractor} and \S\ref{Embedding adaptation via Transformer} respectively. 
%
Finally, we explain how to leverage the class-specific information to decouple the embeddings in \S\ref{Label enhanced decoupling prototypical networks} 



\subsection{Problem definition}
\label{Problem definition}
Given a dataset for intent recognition with $N_D$ utterance-label pairs, i.e., $\mathcal{D}=\{(x_i,y_i)\}_{i=1}^{N_{D}}$, where $x_i$ denotes an utterance and $y_i$ denotes its intent label in a set of classes, i.e., $y_i \in C$.
Hence, the intent recognition task can be formulated as a classified function $\mathcal{F}$ that satisfies $\mathcal{F}(x_i) \mapsto y_i$.
However, the number of labeled data is often not enough to support the training for {optimizing the model to generate the satisfied results}.
%
Instead, few-shot learning can well deal with {such over-fitting problem that the model has a high accuracy on the training set and a low accuracy on the test set in the low-resource scenario.}
%
In addition,  few-shot learning can 
extracts prior experience utterance-label
%that allows quick \chen{generation} on new tasks \cite{DBLP:phd/basesearch/Finn18}.
%The learned prior experience often includes meta knowledge which can be generalized to different tasks, such as the similarity metric and the model architecture.  
%%
%On the new task, few-shot model uses these prior 
as knowledge and a few labeled examples as a \textit{support set}, which are then used to train and predict the label of unseen items called \textit{query set}.


Following the definition of few-shot learning \citep{DBLP:conf/nips/SnellSZ17}, 
a set of classes {$C$} can be divided into the \textbf{base classes} $C_b$ for the training phase and the \textbf{novel classes} $C_n$ for the test phase, where $C_b\cup C_n=C$ and $C_b\cap C_n=\emptyset $.
%
In addition, quantities of meta tasks are required for 
few-shot intent recognition.
%
A meta task $\mathcal{T}$ (or called "episode"\cite{DBLP:conf/nips/SnellSZ17}) usually consists of  two components: a support set and a query set, i.e., $\mathcal{T=\{S,Q\}}$. 
%
The support set $\mathcal{S}=\{(x_s^{(i)},y_s^{(i)})\}_{i=1}^{N_S}$ is a set of $N_S$ utterance-label pairs. The query set $\mathcal{Q}=\{(x_q^{(i)},y_q^{(i)})\}_{i=1}^{N_Q}$ is  a set of $N_Q$ utterance-label pairs, where the intent labels $\mathbf{y}$ need to be predicted.
%
Generally, an "N-way K-shot" meta task means that the support set $\mathcal{S}$ contains $N$ intent labels (i.e., $N$-way) and for each intent, there are $K$ intent samples (i.e. $K$-shot), s.t. $N_S = N*K$.

%
For few-shot intent recognition models, they are usually trained on a set of meta-tasks $\mathcal{D}_{train}=\{\mathcal{T}_{\textit{train}}^{(i)}\}_{i=1}^{N_{train}}$ {depending on the base classes $C_b$},
% 
and are tested on another set of meta tasks $\mathcal{D}_{test}=\{\mathcal{T}_{\textit{test}}^{(i)}\}_{i=1}^{N_{test}}$ { depending on the novel classes $C_n$} without fine-tuning.
%
It should be noted that there is no overlap between the label sets in the training and test sets.
%
The capacity of a few-shot intent recognition model is evaluated by its generalization and adaptation ability on the novel tasks, i.e. the performance on the meta tasks in the test stages. 
%
The ultimate goal is to learn a classifier $h(\cdot)$ from {a parameter space named hypothesis space $\mathcal{H}$ }\citep{DBLP:journals/csur/WangYKN20}, which is trained with the training meta-tasks to minimize the cross entropy loss function $\ell(\cdot,\cdot)$ { over those sampled meta-tasks used for testing on a query set in the training set:}
\begin{equation}
	\label{loss1}
	h^*=\mathop{argmin}\limits_{h\in\mathcal{H}}\sum\limits_{{(x^{C_b}_{\mathcal{Q}},y^{C_b}_{\mathcal{Q}})}\in{\mathcal{Q}}}
	{\ell(\hat{y}^{C_b}_{\mathcal{Q}},y^{C_b}_{\mathcal{Q}})}, (\mathcal{Q} \in \mathcal{T}_{train}),
\end{equation}
%
\begin{equation}
	\hat{y}^{C_b}_{\mathcal{Q}} = h(x^{C_b}_{\mathcal{Q}};\mathcal{S}), (\mathcal{S}\in \mathcal{T}_{train}),
\end{equation}
%
where $(x_{\mathcal{Q}}^{C_b},y_{\mathcal{Q}}^{C_b})$ denotes an utterance-label pair in the query set of training meta-task $\mathcal{T}_{train}$; 
%
$\hat{y}^{C_b}_{\mathcal{Q}}$ is the label predicted by the classifier $h(\cdot)$ with knowledge from support set $\mathcal{S}$ in current training meta-task $\mathcal{T}_{train}$;
%
and $\ell(\cdot,\cdot)$ is a loss function for computing the difference between the predicted {probability distribution} $\hat{y}^{C_b}_{\mathcal{Q}}$ and the ground truth {distribution} ${y}^{C_b}_{\mathcal{Q}}$.

\subsection{Contrastive task adaptation framework}
\label{Task adaptive few-shot intent recognition framework}
%
Traditional few-shot learning models mainly rely on the sufficient prior knowledge to obtain a feature extractor, which can improve their generation and adaptation abilities. 
%
%The key assumption is that the embeddings learned from the few-shot feature extractor \chen{could capture all necessary as well as discriminative representations of data so that simple recognizers are sufficed. }
%
However, we argue that each meta-task with novel classes has its unique information, which cannot be captured by the extracted knowledge learned from the base classes. 
%
Thus, we design a task adaptation module to effectively capture the task-specific information for the current task.
%
In addition, as the task adaptation module will hurt the uniqueness of each category in the meta-task,
%\chen{since the interaction between each category and the rest of categories brings their information in such category embedding}.
%To solve the problem of categories confusion and preserving categories individuality in current meta-task, 
we design a {contrastive} loss to capture features of each category for decoupling the mixed category representations.
%
The overall framework of our proposal is shown in Fig. \ref{Task adaptive few-shot intent recognition framework}, 
which has the following key modules.
%

\textbf{Feature Extractor}. Given an intent instance and its label, 
%neural networks are utilized to embed the input text into a vector space, which contains the semantic information of input data.
%
the BERT model \citep{DBLP:conf/naacl/DevlinCLT19} is employed as the feature extractor to encode text.
%
To fit the specific feature extractor for the few-shot settings, a contrastive learning strategy is applied to generate well-separated embeddings of different categories, which makes the initial BERT model can scratch the unique characteristics of input samples after a rapid fine-tuning.
%Compared with the vector representation obtained from vanilla fine-tuned BERT, the output from our feature extractor has more obvious class-specific features, i.e., the separability of each category. 

\textbf{Task Adaptation Module}. The obtained instance embeddings are based on the prior knowledge from a set of base classes, which is not relevant to current meta-task. 
%Thus we calculate the task-relevant representations for the current meta-task based on the task-agnostic embeddings generated from the feature extractor with the task adaption module.
%
To deal with it, 
we utilize a self-attention layer in Transformer as the task-adaptation module to scratch the global information from the input context, i.e., a set of n-way k-shot instances in current meta-task.
%

%
\textbf{Prototypical Networks for Decoupling}. The instance embeddings in the support set generated by the task adaptation module may carry the information of the rest classes in current meta-task.
%, leading to confusion for  classifying the categories. 
%
To solve it, 
%that the uniqueness of each class may be weakened through the interaction process with different classes, 
we add the semantic representations of label name to the 
instance embeddings
to diminish the distance between embeddings of {instances in the support set} from the same categories in the embedding space.
%Hence, it utilize the k-nearest neighbors idea \citep{Cover1967NearestNP} to predict the labels of queries.

{Finally, we apply the idea of k-nearest neighbors~\citep{Cover1967NearestNP} to predict the labels of queries.}

\subsection{Feature Extractor}
\label{Contrastive learning based feature extractor}
%
Given an n-word utterance $ x=\{w_1, w_2,..., w_n\}$ sampled from the natural dialogs, BERT is first employed as a feature extractor to encode the raw sentence into a continuous low-dimensional vector $f(x)$, which is then 
sent for contrastive learning. 
%which contains necessary semantic information.
%
%In particular, the feature extractor is first trained on meta tasks in base classes to capture the general knowledge that can help \chen{few-shot learning models} generalize to meta tasks in novel classes.
%However, the general knowledge learned from the set of base classes usually cannot well adapt to the meta tasks in novel classes.

Then, to reformulate the meta-task uniqueness for the novel classes, we design a contrastive learning based feature extractor, which pre-trains BERT to obtain the task-uniqueness embeddings of instances in the novel classes
in the support set.
%
In detail, given a set of N-way K-shot samples, we compute the center $v_i$ of a particular class $i$ of samples, which is also called the prototype of class $i$ as:
\begin{equation}
	\label{center}
	v_i = \dfrac{1}{K}\sum_{l=1}^{K}{f(x_i^{(l)})}, (i=1,2,\cdots,N)
\end{equation}
where $x_i^{(l)}$ represents the utterance $l$ in the class $i$.
%
%The goal of the feature extractor is that the similarity between two similar classes should be larger than two dissimilar classes.
%
By doing so, the samples in the same class can have the shortest mean distance to its center \citep{DBLP:journals/jmlr/BanerjeeMDG05, DBLP:conf/nips/SnellSZ17}.

%
%Since that the mean point of all the data points in the same class must have the shortest mean distance from them in space \citep{DBLP:journals/jmlr/BanerjeeMDG05, DBLP:conf/nips/SnellSZ17}, we choose the mean embedding vector to represent a class.
%

 
%
{Our goal is 
to make the similarity of the instance embeddings from the same category far greater than the similarity of instance embeddings from different categories, which is formulated as:}
\begin{equation}
	\label{contrastive}
	s(p^+,p) \gg s(p^-,p),
\end{equation}
where $s(\cdot ,\cdot)$ is a score function measuring the similarity between two samples, $p^+$ is the positive sample which belongs to the same category as $p$ while $p^-$ is the negative sample which belongs to  different categories from $p$.
In this paper, a normalized dot product is chosen as the score function here. For instance, 
\begin{equation}
	s(p^+,p) =\frac{{p^+}^T p}{\Vert p^+\Vert \Vert p\Vert}.
\end{equation}
 
After that, a softmax classifier which can distinguish the positive and negative samples can be constructed,
%
where the loss function should encourage the score function 
$s(\cdot ,\cdot)$
 to assign large values to the positive examples and small values to the negative instances. 
 %
  Thus we construct the loss as:
\begin{equation}
		\label{con}
\begin{split}
		&\mathcal{L}_{con1}= -\mathbb{E}\left[\log \frac{\exp \left( s(p^+,p_i) \right)}{\exp \left( s(p^+,p_i)\right) + \sum_{j=1}^{N-1} \exp \left( s(p_j^-,p)\right) } \right], \\
&(i=1,2,\cdots,N).
\end{split}
\end{equation}
%
The denominator term in Eq. \eqref{con} involves one positive sample, i.e., $\exp \left( s(p^+,p_i)\right)$ and $N-1$ negative samples, i.e., {$\sum_{j=1}^{N-1} \exp \left( s(p_j^-,p_i)\right)$}. 

Considering the computational convenience, we regard the center of the class itself as the positive samples and others as the negative samples.
%
In this way, the optimization goal becomes to minimize the 
similarity of prototypes of different categories, i.e., 
\begin{equation}
min~s(v_i, v_j), (i\neq j).
\end{equation}

Then the loss function Eq. \eqref{con} can be written as:
\begin{equation}
	\label{con1}
	\mathcal{L}_{con1}= -\mathbb{E}\left[\log \frac{\textit{e}}{\textit{e} + \sum_{j=1}^{N} \exp \left( s(v_j,v_i)\right) } \right], (i=1,2,\cdots,N, i\neq j),
\end{equation}
%
where \textit{e} is a constant. 
With the contrastive loss $\mathcal{L}_{con1}$, we expect that the current feature extractor can converge faster than the traditional ones without a contrastive loss in the few-shot scenario.

\subsection{Task adaptation module}
\label{Embedding adaptation via Transformer}
%
%If the feature extractor trained on base classes is directly transferred to novel classes, the embeddings of novel instances obtained from the feature extractor will be task agnostic. 
%%
%Thus, in this section, we introduce a task adaptation module to make the embeddings contain information related to the current meta-task.
%

%Therefore, a logical idea is modeling the embeddings as a sequence so that the set of embeddings can be input to a sequential model including Bidirectional LSTM (Bi-LSTM)\cite{hochreiter1997long}.
%It is notable that the output of the sequential model depends on the order of the input token set.
%However, it is obvious that there is no order of embeddings obtained from the feature extractor in a meta-task, so it is unreasonable to use the sequential model hastily.
%{Information about the sequence order forcibly added by these model is redundant,for the order of each token is unnecessary \textit{\textbf{weights}} interfering the classifier. Because sequential information cannot represent global structure of current meta-task.}
%
Given the N-way K-shot meta tasks, we introduce a task-specific function \citep{DBLP:conf/cvpr/YeHZS20} $\textbf{T}$ which can extract the task-specific information for each meta task.
%
Such function transforms a set of prototypes into the task-specific prototypes. 
Specifically, this process can be formulated as:
{\begin{equation}
		\label{trasformer}
		\underbrace{\{\psi_v; \forall v\in \mathcal{V}_{train}\}}_{task-specific  \ prototypes} = \textbf{T}(\underbrace{\{v;\forall v\in \mathcal{V}_{train}\}}_{ prototypes \ set}),
	\end{equation}
	where $\mathcal{V}_{train} =\{v_1, \cdots, v_N\}$ is a set of prototypes generated by Eq. \eqref{center} in the support set $\mathcal{S}$. }
%
Intuitively, the interaction in the prototype set $\{v;\forall v\in \mathcal{V}_{train}\}$ can reflect the essence of this meta task, i.e., the task-specific information like the categories  and the samples included in each category.
%

{To avoid the influence of the order of prototypes,}
%In addition, the output of the self-attention layer from Transformer \cite{DBLP:conf/nips/VaswaniSPUJGKP17} shown in Eq.\eqref{transformed_result} is insensitive to the input positions, which can better represent the pure interaction information among all inputs, compared with other network architectures \chen{such as Matching Networks \citep{DBLP:conf/nips/VinyalsBLKW16}. Because the Bi-LSTM in Matching Networks can be affected by the order of input.}
%Hence, in this paper, 
we employ a multi-head self-attention to model the task-specific information for each meta task.
%to utilize a multi-head self-attention layer from Transformer \cite{DBLP:conf/nips/VaswaniSPUJGKP17} to .}
%
In detail, 
%extra position encoder in Transformer is added because the self-attention layer cannot record order information.
%Under the current task setting, the interaction relationship of embeddings in each category is the focus of task-specific information.
%
the core of the multi-head self-attention layer is a group of triplets in the form of (query $Q$, key $K$, and value $V$). 
%
{To obtain the weight of each linear projected prototype in the transformed embeddings, we input the triplets 
	into a linear project layer to get the corresponding representations of $Q'$, $K'$ , $V'$, which can be formulated as follows:}
%
\begin{equation}
	\left\{\begin{array}{ll}
		Q' = W_Q^\top [v_1:v_{\left| Q\right|}] \\
		K' = W_K^\top [v_1:v_{\left| K\right|}] \\
		V' = W_V^\top [v_1:v_{\left| V\right|}] ,
	\end{array}\right.
\end{equation} 
where $W_Q, W_K$ and $W_V$ are the linear projected matrices, respectively;  $[:]$ generates a matrix. For instance, $[v_1:v_{\left| Q\right|}]$ produces a matrix composed of the prototypes $v_1 \cdots v_{\left|Q\right|}$; 
%$\left|Q\right|$ means the number of column in matrix $Q$.
% to generate the corresponding matrix.
%And the same operation is used for compute $Q$ and $V$ with $\mathsf{Q}, W_Q$ and $\mathsf{V}, W_V$, respectively.
%

We follow the self-attention algorithm \citep{DBLP:conf/cvpr/YeHZS20} to obtain the final transformed prototypical embeddings $\psi_{v_q}$ for a prototype $v_q$
which incorporates the task-specific information.
%
In detail, we calculate the weighted sum of each column in $V'$ to obtain the task-specific information 
as 
%
\begin{equation}
	\label{transformed_result}
	\psi_{v_q} = v_q + \underbrace{\sum_k{\alpha_{qk}V'_{:,k}},}_{	task-specific \ information
	} \quad \forall v_q \in Q,
\end{equation}
where the weight $\alpha_{qk} $ measures the proximity of the key to the query \citep{DBLP:conf/cvpr/YeHZS20} and reflects the interactions between the prototype $v_q$ and other prototypes.
It can be calculated by  
\begin{equation}
	\alpha_{qk} = \exp{\left( \dfrac{v_{q}^\top W_Q \cdot K'}  {\sqrt{d}}\right) }
	\label{eq9}
\end{equation}
with $V'_{:,k}$ indicating the $k$-th column of $V'$ and $d$ denoting the dimension of the embeddings.
%
Specially, following the initial setting of self-attention mechanism \citep{DBLP:conf/nips/VaswaniSPUJGKP17}, we set $Q=K=V=[v_1, \cdots, v_N]$ in the support set $\mathcal{S}$.

%The self-attention layer computes what is the right value for a query point \textemdash the query $x_q \in \mathsf{Q}$ is the first matched against a list of keys $K$ where each key has a value $V$.
%

%If the mean vector of sample embedding fo each class is utilized as the tokens input to the self-attention layer, the output attention score will be the relationship between each category and others.
%
%In a few-shot meta-task, the above-mentioned interactions can be treated as the so-called task-specific information.
%

\subsection{Prototypical networks for decoupling}
\label{Label enhanced decoupling prototypical networks} 
%
After the task adaptation module, we denote the transformed 
prototype $c_i $ as the center of embeddings of sample in category $i$
%
%
%The main idea of prototypical networks is to use one vector named prototype to represent each intent class. 
%%
%\chen{Prototype $c_i $, or the center of embeddings in category $i$ after being transformed by task adaptation module in \S \ref{Task adaptive few-shot intent recognition framework}} is defined 
as:
\begin{equation}
	\label{prototype}
	c_i= \psi_{v_i}, (i=1,\cdots, N),
\end{equation}
%
where $\psi_{v_i}$ can be produced by Eq. \eqref{eq9} for intent class $i$ in the support set $\mathcal{S}$.

%
%To leverage the task-specific information, \citet{DBLP:conf/cvpr/YeHZS20} calculate prototypes with the transformed embeddings directly.
%%
%Although those transformed embeddings contain task-specific knowledge,
%such method brings the risk of coupling different categories due to the interactions between all categories in current meta-task, which compromise the uniqueness of each category embedding.
%%
%Therefore, some identified features need to be introduced so as to make the boundaries between potential confusing embeddings clear again. 
%That is to say, such features should be typical as well as unique for this category.
%%
%Under the few-shot scenario, there are limited training samples of each category in a meta-task, which makes it difficult to find the common semantics characteristics of a category.
%It is difficult to discover the semantic commonalities among a small number of samples in each class to make the whole model adapt to such meta-task. 
%

%We decide to represent the classes with class-specific characteristics to preserve the uniqueness of a class. 

{In order to well develop the characteristics of the category itself, }
we propose that the label names which contain the category-specific semantics can be used as the prior knowledge  \citep{DBLP:conf/acl/HenaoLCSWWZZ18}.
%
Therefore, we directly use the embeddings of the label names as the guided information of each category and then represent each class by both the label name and {the transformed embeddings of samples in the support set.}
%
That is, for label $y_i$, we compute its semantics-mixed prototype $\tilde{c}_i$ by 
introducing a trade-off factor $\alpha\in[0,1]$ to control the corresponding contributions from the label name and the transformed embeddings as:
\begin{equation}
	\label{decouple}
	\tilde{c}_i=\alpha \times c_i + (1-\alpha) \times f(y_i), (i=1,\cdots,N),
\end{equation}
where $f$ is the encoder like BERT and $\boldsymbol{c}_i$ is the prototypical expression obtained by Eq. \eqref{prototype}. 
%
Here, the label name embeddings $f(y_i)$ serves as a \textit{correction} for the prototypical representation vector. This \textit{correction} can separate the samples in {the meta task} from each other and thus help to clearly express the intent semantics.
%Even if adding label name semantics will weaken the task-specific information brought by the task adaptation module, it will enhance the separability between different categories, which is considered effective and necessary.
%

With the transformed embeddings $\psi_v$, the utterance $x_{\mathcal{Q}}$  in the query set $\mathcal{Q}$ can be classified according to the transformed embeddings of the prototypes in support set $\mathcal{S}$ \cite{DBLP:conf/cvpr/YeHZS20}:
%
\begin{equation}
	\label{classifier}
	\hat{y}_{\mathcal{Q}}=h(x_{\mathcal{Q}};\{\tilde{c}_i, i \in 1 \cdots, N\}),
\end{equation}
%
where $h$ is a classifier to predict the label of $x_{\mathcal{Q}}$ based on the semantics-mixed prototypes and $\hat{y}_{\mathcal{Q}}$ is the predicted result.
%
Besides, to guarantee that the adapted instance embeddings are similar to {the neighbors from the same class} and dissimilar to those from different classes, we introduce another loss as:
\begin{equation}
	\label{con2}
	\mathcal{L}_{con2}= \ell\left( softmax\left(sim(\psi_{x_{\mathcal{Q}}}, \boldsymbol{\tilde{c}}^*)\right),y_{\mathcal{Q}}\right),
\end{equation}
%
where $\psi_{x_{\mathcal{Q}}}$ is the embeddings of $x_{\mathcal{Q}}$ after being task adapted; ${\tilde{c}}^{*}$ is the corresponding semantics-mixed prototype of the ground truth label; $\ell(\cdot,\cdot)$ is the same loss function mentioned in Eq. \eqref{loss1}.
%
{We choose the cosine similarity here as the distance function $sim(\cdot,\cdot)$ to evaluate the similarity of two instances in the embedding space.}
%
Finally, the final objective function is designed as:
\begin{equation}
	\label{totalloss}
	\begin{split}
		\mathcal{L}(\hat{y}_{\mathcal{Q}},y_{\mathcal{Q}})&=\ell(\hat{y}_{\mathcal{Q}},y_{\mathcal{Q}})\\
		&+\lambda\cdot\mathcal{L}_{con1}+(1-\lambda)\cdot\mathcal{L}_{con2} ,
	\end{split}
\end{equation}
where 
{$\ell(\hat{y}_{\mathcal{Q}},y_{\mathcal{Q}})$, $\mathcal{L}_{con1}$ and $\mathcal{L}_{con2}$ are generated based on Eq.~\eqref{loss1}, Eq.~\eqref{con1} and Eq.~\eqref{con2}, respectively.}
%
$\mathcal{L}_{con1}$ is utilized for optimizing the feature extractor and $\mathcal{L}_{con2}$ is used for optimizing the task adaptation module. 
%
$\lambda$ is a coefficient to balance the contribution of the contrastive learning objective function and the classification objective function.
%
The detail process of the CTA model can refer to Algorithm \ref{alg:Framwork}.


%
\begin{algorithm}[t]
	\caption{ Task adaptation contrastive learning. }
	\label{alg:Framwork}
	\begin{algorithmic}[1]
		\Require
		The set of base classes for training, $C_b$;
		the set of original training data $\mathcal{D}_{origin}$;
		the max iteration, $iter_{max}$
		\Ensure
		Optimal classifier ${h}$;
		optimal feature extractor ${f}$.
		\State $i=0$;
		\While{$i<iter_{max}$}
		\State Sample training meta-tasks $\mathcal{D}_{train}$ from  $\mathcal{D}_{origin}$;
		\ForAll{$\mathcal{T}_{\textit{train}}^{(j)} \in \mathcal{D}_{train}$}
		\State Obtain $f(x), f(y)$ with $\{x, y\}$ from $\mathcal{T}_{\textit{train}}^{(j)}$;
		\State Compute the contrastive loss of the feature extractor $\mathcal{L}_{con1}$ with Eq. \eqref{con1};
		\label{code:fram:add}
		\State Obtain $\psi_v$ from the support set $\mathcal{S} $ in $ \mathcal{T}_{\textit{train}}^{(j)}$ with $\textbf{T}$;
		\State Compute the semantics-mixed prototype $\tilde{c}$  with Eq. \eqref{decouple};
		\State Compute the label $\hat{y}_\mathcal{Q}$ of instances via Eq. \eqref{classifier};
		\State Compute the total loss $\mathcal{L}(\hat{y}_{\mathcal{Q}},y_{\mathcal{Q}})$ with Eq. \eqref{totalloss};
		\EndFor
		\State Compute $\nabla_{f,\textbf{T}} \sum_{\mathcal{T}_{train} \in \mathcal{D}_{train}} \mathcal{L}(\hat{y}_{\mathcal{Q}},y_{\mathcal{Q}})$;
		\State Update $f$ and $\textbf{T}$ with $\nabla_{f,\textbf{T}}$;
		\State $i=i+1$;
		\EndWhile
		\label{code:fram:select} \\
		\Return Optimal classifier ${h}$;
		optimal feature extractor ${f}$.
	\end{algorithmic}
\end{algorithm}